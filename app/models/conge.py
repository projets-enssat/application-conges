from app.server import database
from sqlalchemy import Column, String, Integer, ForeignKey, Date
from sqlalchemy.orm import relationship

from datetime import datetime


class CongesInsuffisants(Exception):
    pass
    

class MOTIF_CONGE:
    RTT = "RTT"
    ANNUEL = "Congé annuel"
    ENFANT_MALADE = 'Enfant malade'
    DECES_PROCHE = "Décès d'un proche"


class STATUT_CONGE:
    VALIDE = "validé"
    REFUSE = "refusé"
    EN_ATTENTE = "attente de validation"


class Conge(database.Model):
    id = Column(Integer, primary_key=True)

    # associe le congé à un employé
    id_employe = Column(ForeignKey('employe.id'), nullable=False)
    # recupere l'objet Employe en cascade
    employe = relationship('Employe', back_populates="conges")

    motif = Column(String)
    date_debut = Column(Date)
    date_fin = Column(Date)
    statut = Column(String)
    commentaire = Column(String)

    def est_traite(self):
        return self.statut != STATUT_CONGE.VALIDE and self.statut != STATUT_CONGE.REFUSE
   

    def get_date_debut(self):
        return self.date_debut.strftime('%d/%m/%Y')

    def get_date_fin(self):
        return self.date_fin.strftime('%d/%m/%Y')
