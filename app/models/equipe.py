from app.server import database
from sqlalchemy import Column, String, Integer


class Equipe(database.Model):
    __tablename__ = 'equipe'
    # cle primaire pour la table equipe
    id = Column(Integer, primary_key=True)

    nom = Column(String, unique=True)

    def ajouterEmploye(self):
        """
        Inutile ici car en ajoutant l'equipe a l'employe,
        SQLAlchemy rajoute l'employe à la liste des employes de l'Equipe
        """

    def supprimerEmploye(self):
        """
        Inutile ici car en supprimant un employe,
        SQLAlchemy enleve l'employe de la liste des employes de l'Equipe
        """
