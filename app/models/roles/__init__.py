from .utilisateur import Utilisateur
from .employe import Employe
from .rh import RH
from .rrh import RRH

ROLES = {
    'employe': {
        'nom': 'Employé',
        'classe': Employe
    },
    'rh': {
        'nom': 'RH',
        'classe': RH
    },
    'rrh': {
        'nom': 'Responsable RH',
        'classe': RRH
    }
}
