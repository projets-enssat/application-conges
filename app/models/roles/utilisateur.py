from app.server import database
from sqlalchemy import Column, String, Integer

class Utilisateur(database.Model):
    # déclaration d'un nom de table à associer au modèle
    __tablename__ = 'utilisateur'

    # attributs de l'utilisateur
    id = Column(Integer, primary_key=True)
    login = Column(String(50))
    password = Column(String(50))

    # le role peut etre : utilisateur / employe / rh / rrh (selon la classe / superclasse instanciee)
    # voir compte rendu pour plus d'explications
    role = Column(String(50))

    # indique à SQLAlchemy de remplir l'attribut fonction comme explique ci-dessus
    __mapper_args__ = {
        'polymorphic_identity': 'utilisateur',
        'polymorphic_on': role
    }

    def connexion(self, password):
        return self.password == password
