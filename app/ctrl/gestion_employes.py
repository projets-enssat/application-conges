from sqlalchemy.exc import IntegrityError

from app.server import server
from app.ctrl.utils import Auth, render, request, abort, redirect, notify_success, notify_error, get_current_user
from app.ctrl.urls import URL

from app.models import Employe, Equipe, RH, RRH
from app.models.roles import ROLES
from app.models.roles.rrh import AdresseMailExistante

@server.route(URL.PAGE.GESTION_EMPLOYES)
@Auth.role_in('rrh')
def ctrl_gestion_rh():
    print(ROLES)
    return render(
        'gestion_employes.html', 
        page='gestion - employes', 
        onglet='gestion_rh',
        employes=Employe.query.all(),
        equipes=Equipe.query.all(),
        ROLES=ROLES
    )

@server.route('/api/employes', methods=['POST'])
@Auth.role_in('rrh')
def ajouter_employe():
    current_user = get_current_user()

    nom = request.form.get('nom')
    prenom = request.form.get('prenom')
    adresse = request.form.get('adresse')
    mail = request.form.get('mail')
    id_equipe = request.form.get('id_equipe')
    role = request.form.get('role')

    try:
        nouvel_employe = current_user.creer_employe(role, nom, prenom, adresse, mail, id_equipe)
        return redirect(URL.PAGE.PREFIXE_FICHE_EMPLOYE + str(nouvel_employe.id))
    except AdresseMailExistante:
        notify_error("Cette adresse mail est déjà utilisée par un autre employé!")
        return redirect(URL.PAGE.GESTION_EMPLOYES)


@server.route('/api/employes/delete', methods=['POST'])
@Auth.role_in('rrh')
def supprimer_employe():
    current_user = get_current_user()

    id_employe = request.form.get('id_employe')

    current_user.supprimer_employe(id_employe)
    notify_success("L'employé a bien été supprimé")

    return redirect(URL.PAGE.GESTION_EMPLOYES)


@server.route(URL.PAGE.MODIFIER_EMPLOYE, methods=['GET'])
def page_modification_employe(id_employe):
    employe = Employe.query.filter_by(id=id_employe).first()
    equipes = Equipe.query.all()
    return render("modifier_employe.html", employe=employe, equipes=equipes)


@server.route(URL.RES.MODIFIER_EMPLOYE, methods=['POST'])
def ressource_modification_employe(id_employe):
    current_user = get_current_user()

    nom = request.form.get('nom')
    prenom = request.form.get('prenom')
    adresse = request.form.get('adresse')
    mail = request.form.get('mail')
    id_equipe = request.form.get('id_equipe')


    try:
        current_user.modifier_employe(id_employe, nom, prenom, adresse, mail, id_equipe)
        notify_success("Employe modifie.")
    except AdresseMailExistante:
        notify_error("Adresse mail existante.")

    return redirect(URL.PAGE.GESTION_EMPLOYES)
