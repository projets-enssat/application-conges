from datetime import datetime

from app.models import Utilisateur
from flask import session, render_template, request, redirect, abort
from app.config import app_env
from app.ctrl.urls import URL
from app.auth import Auth


def date(string):
    return datetime.strptime(string, '%d/%m/%Y')


def get_current_user():
    session_user = session.get('user')
    current_user = Utilisateur.query.filter_by(id=session_user['id']).first() if session_user else None
    return current_user


def render(template_path, **environment):
    notification = session.get('notification')
    session['notification'] = None

    return render_template(
        template_path, 
        URL=URL, # passe la liste des URLs des pages / ressources de l'app à chaque template
        app=app_env, # passe un objet contenant la config de l'app (nom, hote, port...)
        user=get_current_user(), # passe les informations de l'utilisateur connecté (ou None si pas connecté)
        notification=notification,
        **environment
    )

def notify(texte, ntype=None):
    session['notification'] = dict(
        texte = texte,
        type = ntype
    )

def notify_error(texte):
    notify(texte, 'danger')

def notify_success(texte):
    notify(texte, 'success')
