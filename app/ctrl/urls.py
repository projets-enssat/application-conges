
class URL:
    # URL des pages de l'application
    class PAGE:
        HOME = '/conges/mes_demandes'
        CONNEXION = '/connexion'

        MES_CONGES = '/conges/mes_demandes'
        CONGES_EQUIPE = '/conges/equipe'

        # demandes
        VALIDATION_DEMANDES = "/gestion/demandes"
        
        # pages pour tout les employés
        MODIFIER_CONGE = '/conges/modifier/<string:id_conge>'

        # employes
        GESTION_EMPLOYES = '/employes/consulter'
        MODIFIER_EMPLOYE = '/employes/modifier/<int:id_employe>'

        PREFIXE_FICHE_EMPLOYE = '/employes/fiche/'
        FICHE_EMPLOYE = PREFIXE_FICHE_EMPLOYE + '<int:id_employe>'

        # equipes
        GESTION_EQUIPES = '/equipes/consulter'

        MODIFIER_EQUIPE_PREFIXE = '/equipes/modifier/'
        MODIFIER_EQUIPE = MODIFIER_EQUIPE_PREFIXE + '<int:id_equipe>'
    
    # URLs des ressources de l'API
    class RES:
        DEMANDER_CONGE = '/api/conges/create'

        VALIDATION_CONGE = '/api/validation_conge/'

        SUPPRIMER_CONGE_PREFIXE = '/api/conges/delete/'
        SUPPRIMER_CONGE = SUPPRIMER_CONGE_PREFIXE + '<int:id_conge>'

        MODIFIER_CONGE_PREFIXE = '/api/conges/update/'
        MODIFIER_CONGE = MODIFIER_CONGE_PREFIXE + '<int:id_conge>'

        MODIFIER_EMPLOYE = '/api/employes/update/<int:id_employe>'

        # equipe
        MODIFIER_EQUIPE_PREFIXE = '/api/equipes/update/'
        MODIFIER_EQUIPE = MODIFIER_EQUIPE_PREFIXE + '<int:id_equipe>'

        SUPPRIMER_EQUIPE_PREFIXE = '/api/equipes/delete/'
        SUPPRIMER_EQUIPE = SUPPRIMER_EQUIPE_PREFIXE + '<int:id_equipe>'
        
        # connexion / deconnexion
        CONNEXION = '/api/connexion'
        DECONNEXION = '/api/deconnexion'
