from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_session import Session
from .config import app_conf

# instanciate a flask web server
server = Flask('gestion_conges', template_folder='views')
server.secret_key = 'my secret key'
server.config.from_object(app_conf)

database = SQLAlchemy(server) if True else False

